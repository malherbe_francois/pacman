/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author mad
 */
public class Personnage {
    
    protected Position pos = null;
    
    public Position getPosition () {
        return this.pos;
    }
    
    public int getI () {
        return this.pos.getI();
    }
    
    public int getJ () {
        return this.pos.getJ();
    }
    
    public void deplace (Direction d) {
        this.pos.move(d);
    }
}
